//
//  main.cpp
//  OpenGlLab
//
//  Created by Md. Abdul Munim Dibosh on 1/21/14.
//  Copyright (c) 2014 Md. Abdul Munim Dibosh. All rights reserved.
//

#include <iostream>														// Header File For NeHeGL
#include "helpers.h"
#include "Physics2.h"	
#include "Beautifier.h"

GLdouble width, height;   /* window width and height */

#define ORANGE 239/255.0,110/255.0,18/255.0
#define WHITE 0.9,0.9,0.9
#define CYAN 77.0/255.0,184.0/255.0,235.0/255.0
#define GREEN 0,1,0
#define RED 1,0,0
#define SKY_BLUE 83.0/255.0,171.0/255.0,238.0/255.0
//COLOR OF LIGHT
const GLfloat light_ambient_white[]  = { WHITE, 1.0f };//day
const GLfloat light_ambient_cyan[]  = {CYAN, 1.0f };
const GLfloat light_ambient_cyan_light[]  = {SKY_BLUE, 1.0f };
const GLfloat light_ambient_orange[]  = {ORANGE, 1.0f };
const GLfloat light_ambient_green[]  = {GREEN, 1.0f };
const GLfloat light_ambient_red[]  = {RED, 1.0f };

int light_color_choice = 1;//default
float LightX=-1,LightY=-1,LightZ=0;
//LIGHT0
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position[] = {LightX,LightY,LightZ, 1.0f };

//initial camera angles
float radius = 4;

float theta =90;
//rope connector texture angle
float rotate;
//rope movement
float increment = 10.0f;
//current time
int current_milliseconds;
int last_milliseconds;
//duration of animatioin
long duration = 1000;

int noOfMasses = 40;
float perParticleWeight = 0.05f;//kg
float weightForLastObject = 2.0f;//kg

int rotateCamera = 0;

GLuint _textureId; //The id of the texture

GLUquadric *quad;


RopeSimulation* ropeSimulation = new RopeSimulation(noOfMasses,						// Particles (Masses)
													0.05f,                  //per particle(mass) weight in kg
                                                    Vector3D(0, -9.81f, 0), // Gravitational Acceleration
													-1.5f                   // Height Of Ground
                                                    ,weightForLastObject);                  // Weight for the object at loose end

/* Program initialization NOT OpenGL/GLUT dependent,
 as we haven't created a GLUT window yet */
void init(void)
{
    width  = DEFAULT_WIDTH;                 /* initial window width and height, */
    height = DEFAULT_HEIGHT;
       
}
//updates the scene
void update(int milliseconds){
    
    float dt = milliseconds / 1000.0f;
    
	float maxPossible_dt = 0.002f;//max allowed difference
    
  	int numOfIterations = (int)(dt / maxPossible_dt) + 1;					// Calculate Number Of Iterations To Be Made At This Update Depending On maxPossible_dt And dt
	if (numOfIterations != 0)												// Avoid Division By Zero
		dt = dt / numOfIterations;											// dt Should Be Updated According To numOfIterations
    
	for (int a = 0; a < numOfIterations; ++a){
		ropeSimulation->operate(dt);
    }
    
    Vector3D initialConnectionPointVelocity;
    initialConnectionPointVelocity.x = initialConnectionPointVelocity.z = initialConnectionPointVelocity.y;
    ropeSimulation->setRopeConnectionVel(initialConnectionPointVelocity);
}

void show2DScene()
{
    glColor3f(1.0f, 0.0f, 0.0f);
    float x = 50;
    drawText("METALIC CHAIN SIMULATION",LARGE_TEXT,x, screen_height-50);
    drawText("Md. Abdul Munim Dibosh | Irteza Nasir | Mir Tazbinur Sharif",MEDIUM_TEXT,x, screen_height-65);
}
/* Callback functions for GLUT */

/* Draw the window - this is where all the GL actions are */
void display(void)
{
    /* set the screen clearing color */
    glClearColor(250/255.0f, 247/255.0f, 237/255.0f, 0);//white
    
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    
    //2d text
    glColor3ub(255, 0, 0);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0.0,screen_width, 0.0, screen_height);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    
    show2DScene();
    
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glEnable(GL_TEXTURE_2D);
    //BACK TO 3DSPACE
    initModes(screen_width, screen_height);
    
    
    float pX = radius*cos(theta);
    float pY = 0;
    float pZ = radius*sin(theta);
    float lX = 0;
    float lY = 0;
    float lZ = 0;
    float upX = 0;
    float upY = 1;
    float upZ = 0;
    gluLookAt(pX,pY,pZ,lX,lY,lZ,upX,upY,upZ);
    //lighting
    switch (light_color_choice) {
            
        case 1:
            glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient_white);
            break;
        case 2:
            glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient_orange);
            break;
        case 3:
            glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient_cyan);
            break;
        case 4:
            glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient_green);
            break;
        case 5:
            glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient_red);
            break;
    }
    
    //common settings for light0 
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    
//    glNormal3f(0, 1, 0);
    // Draw A Plane To Represent The Ground (Different Colors To Create A Fade)
	glBegin(GL_QUADS);
        glColor3ub(180, 180,180);												// Set Color To Light Blue
        glVertex3f(20, ropeSimulation->groundHeight, 20);
        glVertex3f(-20, ropeSimulation->groundHeight, 20);
        glColor3ub(80,80,80);												// Set Color To Black
        glVertex3f(-20, ropeSimulation->groundHeight, -20);
        glVertex3f(20, ropeSimulation->groundHeight, -20);
	glEnd();
    // Start Drawing Shadow Of The Rope
	glColor3ub(230,230,230);													// Set Color To Black
	for (int a = 0; a < ropeSimulation->numOfMasses - 1; ++a)
	{
		Mass* mass1 = ropeSimulation->getMass(a);
		Vector3D* pos1 = &mass1->pos;
        
		Mass* mass2 = ropeSimulation->getMass(a + 1);
		Vector3D* pos2 = &mass2->pos;
        
        
        
        if(a%2==0){
            glColor3ub(113,96, 58);
        }
        else{
            // rope color2
            glColor3ub(223, 190, 148);
        }
        float height = 0.001;
        float posY = ropeSimulation->groundHeight;
        if(a==0){
            
            glPushMatrix();{
                glColor3ub(200, 200, 200);
                glTranslatef(pos1->x, posY, pos1->z);
                glScalef(2, height, 0.2);
                glutSolidCube(0.5);
            }glPopMatrix();
        }
        glPushMatrix();{
            glTranslatef(pos1->x, posY, pos1->z);
            glScalef(1, height, 1);
            glutSolidCube(0.05);
        }glPopMatrix();
                
        glPushMatrix();{
            glTranslatef(pos2->x, posY, pos2->z);
            glScalef(1, height, 1);
            glutSolidCube(0.05);
        }glPopMatrix();
        
        if(a==ropeSimulation->numOfMasses-2){
            //the last rigid object
            glPushMatrix();{
                glColor4ub(120,120,120,200);
                glTranslatef(pos2->x, posY, pos2->z);
                glScalef(1, height, 1);
                glutSolidSphere(weightForLastObject/40, 20, 10);
            }glPopMatrix();
        }
        
	}
	// Drawing Shadow Ends Here.
    											// Set Color To Yellow
	for (int a = 0; a < ropeSimulation->numOfMasses - 1; ++a)
	{
		Mass* mass1 = ropeSimulation->getMass(a);
		Vector3D* pos1 = &mass1->pos;
        
		Mass* mass2 = ropeSimulation->getMass(a + 1);
		Vector3D* pos2 = &mass2->pos;
        //connection point
        
        if(a==0){
            
            glPushMatrix();{
                glColor3ub(10, 250, 10);
                glTranslatef(pos1->x, pos1->y, pos1->z);
                glScalef(2, 0.15, 0.2);
                glutSolidCube(0.5);
            }glPopMatrix();
        }
        
//		glLineWidth(10);
//		glBegin(GL_LINES);
//        glVertex3f(pos1->x, pos1->y, pos1->z);
//        glVertex3f(pos2->x, pos2->y, pos2->z);
//		glEnd();
        // rope color.
        if(a%2==0){
            glColor3ub(113,96, 58);
        }
        else{
            // rope color2
            glColor3ub(223, 190, 148);
        }
        float posY = pos1->y;
        
        glPushMatrix();{
            glTranslatef(pos1->x, posY, pos1->z);
            glRotatef(45,0, 0, 1);
            glutSolidCube(0.05);
        }glPopMatrix();
        
        posY = pos2->y;
        
        glPushMatrix();{
            glTranslatef(pos2->x, posY, pos2->z);
            glRotatef(45, 0, 0, 1);
            glutSolidCube(0.05);
        }glPopMatrix();
        
        if(a==ropeSimulation->numOfMasses-2){
            //the last rigid object
            glPushMatrix();{
                glColor4ub(120,120,120,200);
                glTranslatef(pos2->x, pos2->y, pos2->z);
                glutSolidSphere(weightForLastObject/40, 20, 10);
            }glPopMatrix();
        }
        
        
	}
	// Drawing The Rope Ends Here.
    
    glutSwapBuffers(); //swap the bufferss
    
}


void updateConnectionPoint(Vector3D ropeConnectionVel){
    
    printf("X:%f,Y:%f,Z=%f\n\n",ropeConnectionVel.x,ropeConnectionVel.y,ropeConnectionVel.z);
    ropeSimulation->setRopeConnectionVel(ropeConnectionVel);				// Set The Obtained ropeConnectionVel In The Simulation
}
//KEYBOARD CALLBACK
void CallBackKeyboardFunc(unsigned char key, int x, int y)
{
    //the connection point
    Vector3D ropeConnectionVel;
    
    
    switch (key) {
        case 'i':
            ropeConnectionVel.z -= increment;// Add Velocity In -Z Direction
            break;
        case 'o':
            ropeConnectionVel.z += increment;										// Add Velocity In +Z Direction
            break;
        case 'w':
            weightForLastObject+=2.0f;
            if(weightForLastObject > 10)weightForLastObject = 10.0f;
            break;
        case 's':
            weightForLastObject-=2.0f;
            if(weightForLastObject < 2)weightForLastObject = 2.0f;
            break;
        case 'c':
            if(rotateCamera == 0)rotateCamera = 1;
            else rotateCamera = 0;
            break;
        case '1':
            light_color_choice = 1;
            break;
        case '2':
            light_color_choice = 2;
            break;
        case '3':
            light_color_choice = 3;
            break;
        case '4':
            light_color_choice = 4;
            break;
        case '5':
            light_color_choice = 5;
            break;
        case 27:		//ESC
            exit(0);
            break;
    }
    updateConnectionPoint(ropeConnectionVel);
}
//SPECIAL KEY CALLBACK
void CallbackSpecialKeyboardFunction(int key, int x,int y){
    
    //the connection point
    Vector3D ropeConnectionVel;
    switch (key) {
        case GLUT_KEY_LEFT :
            ropeConnectionVel.x -= increment;// Add Velocity In -X Direction
            break;
        case GLUT_KEY_RIGHT :
            ropeConnectionVel.x += increment;// Add Velocity In +X Direction
            break;
        case GLUT_KEY_DOWN :
            ropeConnectionVel.y -= increment;										// Add Velocity In -Y Direction
            break;
        case GLUT_KEY_UP :
            ropeConnectionVel.y += increment;										// Add Velocity In +Y Direction
            break;
    }
    updateConnectionPoint(ropeConnectionVel);
}


/* Called when window is resized,
 also when window is first created,
 before the first call to display(). */
void reshape(int w, int h)
{
    /* save new screen dimensions */
    width = (GLdouble) w;
    height = (GLdouble) h;
    screen_width = width;
    screen_height = height;
    initModes(width,height);
    
}



void animate(){
	/**ANY ANIMATION RELATED STUFFS**/
    
    //CAMERA
    //    theta+=0.5;
    
    current_milliseconds = glutGet(GLUT_ELAPSED_TIME);
    update(current_milliseconds-last_milliseconds);
    last_milliseconds = current_milliseconds;

    if(rotateCamera == 1){
        theta += 0.01;
        if(theta > 360) theta = 0;
    }
    //the rope connector is a sphere so should have the texture wraped around
    
    rotate+=2.0f;
    
    if(rotate>360.f)
        
    {
        
        rotate-=360;
        
    }
    
    //FORCE TO REDRAW
    glutPostRedisplay();
}


int main(int argc,char * argv[])
{

    init();
    screen_height = DEFAULT_HEIGHT;
    screen_width = DEFAULT_WIDTH;
    /* initialize GLUT, let it extract command-line
     GLUT options that you may provide
     - NOTE THE '&' BEFORE argc */
    glutInit(&argc, argv);
    
    initWindow("OpenGLTests",width,height,0);//passing 1 will make it fullscreen
    
    //initial viewport setting
    initModes(width, height);
    
    
    //optional
    glEnable( GL_DEPTH_TEST );
    glEnable(GL_POINT_SMOOTH);
    
    /* --- register callbacks with GLUT --- */
    
    /* register function to handle window resizes */
    glutReshapeFunc(reshape);
    
    /* register function that draws in the window */
    glutDisplayFunc(display);
    //do the calculation
    glutIdleFunc(animate);
    //keyboard functioin
    glutKeyboardFunc(CallBackKeyboardFunc);
    glutSpecialFunc(CallbackSpecialKeyboardFunction);

    //enable lighting
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHTING);
    //enable materials
    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 0.15 };
    GLfloat mat_shininess[] = { 100.0 };    
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    
    /* start the GLUT main loop */
    glutMainLoop();
    return 0;
}

